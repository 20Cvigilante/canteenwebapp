<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <body>
        <c:if test="${prompt ne null}">
             <h3><i><c:out value="${prompt}"/></i></h3>
             <form method="${formMethod}" action="${servlet}">
                  <c:forEach items="${items}" var="item" varStatus="loop">
                       <input type="radio" name="${valueType}" value="${item.getId()}"/>
                       <label><c:out value="${item.getName()}"/></label>
                       <br/><br/>
                  </c:forEach>
                  <br/>
                  <input type="submit" value="Submit" />
             </form>
        </c:if>
    </body>
</html>