<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="/styles/styles.css">
        <title>Meal Schedule</title>
    </head>
    <body>
        <h2><i>Meals Schedule</i></h2>

        <jsp:include page="/WEB-INF/util/loopItems.jsp" />

        <c:if test="${schedulePrintLines ne null}">
            <h3>Schedule for <c:out value="${mealName}"/></h3>
            <table>
                <tr>
                    <th>Day</th>
                    <th>Meal Period</th>
                </tr>
                <c:forEach items="${schedulePrintLines}" var="item" begin="1" end="${schedulePrintLines.size()}" step="2"
                varStatus="loop">
                    <tr>
                        <td><c:out value="${schedulePrintLines.get(loop.index - 1)}"/></td>
                        <td><c:out value="${schedulePrintLines.get(loop.index)}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>

        <br/>
        <a href="<c:url value="/"/>">Return Home</a>
    </body>
</html>