<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <header>
        <link rel="stylesheet" href="/styles/styles.css">
        <title>New Meal</title>
    </header>
    <body>
        <h2>New Meal at Therap Canteen</h2>

        <c:if test="${prompt ne null}">
            <h3><i><c:out value="${prompt}"/></i></h3>
            <form action = "/newMeal" method="post">
                <input type="text" name="${valueType}" value="${mealName}"><br>
                <input type="submit" value="Submit">
            </form>
        </c:if>

        <c:if test="${mealName ne null}">
            <p><i><c:out value="${operationResult}"/></i></p>
        </c:if>

        <br/>
        <a href="<c:url value="/"/>">Return Home</a>
    </body>
</html>