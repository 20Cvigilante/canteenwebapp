<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
          <link rel="stylesheet" href="/styles/styles.css">
          <title>All Meals</title>
    </head>
    <body>
            <h2><i>Meals at Therap Canteen</i></h2>
            <table>
                <tr>
                     <th><h3>Meal Names</h3></th>
                </tr>
                <c:forEach items="${meals}" var="item">
                    <tr>
                        <td><h5><c:out value="${item.getName()}"/></h5></td>
                    </tr>
                </c:forEach>
            </table>
            <br/>
            <a href="<c:url value="/"/>">Return Home</a>
    </body>
</html>