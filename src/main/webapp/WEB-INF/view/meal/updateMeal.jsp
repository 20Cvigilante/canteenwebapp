<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
     <head>
          <link rel="stylesheet" href="/styles/styles.css">
          <title>Update Meal Name</title>
     </head>
     <body>
          <h2>Updating Meal Name</h2>

          <jsp:include page="/WEB-INF/util/loopItems.jsp" />

          <c:if test="${newNamePrompt ne null}">
               <h3><i><c:out value="${newNamePrompt}"/></i></h3>
               <form action = "/updateMeal" method="post">
                    <input type="text" name="newName" value="${newName}"><br>
                    <input type="submit" value="Submit">
               </form>
          </c:if>

          <c:if test="${operationResult ne null}">
               <c:forEach items="${operationResult}" var="item" varStatus="loop">
                    <p><c:out value="${item}"/></p>
               </c:forEach>
          </c:if>

          <br/>
          <a href="<c:url value="/"/>">Return Home</a>
    </body>
</html>