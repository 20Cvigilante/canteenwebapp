<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="/styles/styles.css">
        <title>Cancel Meal-Period For Day</title>
    </head>
    <body>
        <h2>Cancel Meal-Period for Day</h2>

        <jsp:include page="/WEB-INF/util/loopItems.jsp" />

        <c:if test="${operationResult ne null}">
            <c:forEach items="${operationResult}" var="item" varStatus="loop">
                <p><c:out value="${item}"/></p>
            </c:forEach>
        </c:if>

        <br/>
        <a href="<c:url value="/"/>">Return Home</a>
    </body>
</html>