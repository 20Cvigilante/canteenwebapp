<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="/styles/styles.css">
        <title>Periods for Day</title>
    </head>
    <body>
        <h2>View Periods for Day</h2>

        <c:if test="${prompt ne null}">
            <h3><i><c:out value="${prompt}"/></i></h3>
            <form method="get" action="/periodsForDay">
                <c:forEach items="${items}" var="item" varStatus="loop">
                    <input type="radio" name="${valueType}" value="${item.getId()}"/>
                    <label><c:out value="${item.getName()}"/></label>
                    <br/><br/>
                </c:forEach>
                <br/>
                <input type="submit" value="Submit" />
            </form>
        </c:if>

        <c:if test="${periods ne null}">
            <h3>Periods for <c:out value="${dayName}"/></h3>
            <table>
                <tr>
                    <th>Periods</th>
                </tr>
                <c:forEach items="${periods}" var="item" varStatus="loop">
                    <tr>
                        <td><c:out value="${item.getName()}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>

        <br/>
        <a href="<c:url value="/"/>">Return Home</a>
    </body>
</html>