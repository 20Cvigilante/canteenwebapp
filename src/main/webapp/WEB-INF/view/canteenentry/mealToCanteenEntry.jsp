<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
     <head>
          <link rel="stylesheet" href="/styles/styles.css">
          <title>Add Meal To Day Period</title>
     </head>
     <body>
          <h2>Add Meal to Day and Period</h2>

          <jsp:include page="/WEB-INF/util/loopItems.jsp" />

          <c:if test="${operationResult ne null}">
               <p><c:out value="${operationResult}"/></p>
          </c:if>

          <br/>
          <a href="<c:url value="/"/>">Return Home</a>
     </body>
</html>