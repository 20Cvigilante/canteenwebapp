<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="/styles/styles.css">
        <title>Menu</title>
    </head>
    <body>
        <h2>Therap Canteen Menu</h2>
        <table>
            <tr>
                <th>Day</th>
                <th>Meal Period</th>
                <th>Meals</th>
            </tr>
            <c:forEach items="${menuPrintLines}" begin="2" end="${menuPrintLines.size()}" step="3" varStatus="loop">
                <tr>
                    <td><c:out value="${menuPrintLines.get(loop.index - 2)}"/></td>
                    <td><c:out value="${menuPrintLines.get(loop.index - 1)}"/></td>
                    <td><c:out value="${menuPrintLines.get(loop.index)}"/></td>
                </tr>
            </c:forEach>
        </table>
        <br/>
        <a href="<c:url value="/"/>">Return Home</a>
    </body>
</html>