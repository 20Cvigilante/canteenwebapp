<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="/styles/styles.css">
        <title>Meals For Day</title>
    </head>
    <body>
        <h2>View Meals for a Day</h2>

        <jsp:include page="/WEB-INF/util/loopItems.jsp" />

        <c:if test="${meals ne null}">
            <h3>Meals for <c:out value="${dayName}"/></h2>
            <table>
                <tr>
                    <th>Meals</th>
                </tr>
                <c:forEach items="${meals}" var="item" varStatus="loop">
                <tr>
                    <td><c:out value="${item.getName()}"/></td>
                </tr>
              </c:forEach>
            </table>
        </c:if>

        <br/>
        <a href="<c:url value="/"/>">Return Home</a>
    </body>
</html>