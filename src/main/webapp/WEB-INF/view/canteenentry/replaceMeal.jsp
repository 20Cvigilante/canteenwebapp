<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
     <head>
          <link rel="stylesheet" href="/styles/styles.css">
          <title>Replace Meal</title>
     </head>
     <body>
          <h2>Replace Meal For a Day and a Period</h2>

          <c:if test="${currMealPrompt ne null && newMealPrompt ne null}">
               <form method="post" action="/replaceMeal">
                    <h3><i><c:out value="${currMealPrompt}"/></i></h3>
                    <c:forEach items="${servedMeals}" var="item" varStatus="loop">
                         <input type="radio" name="currMealId" value="${item.getId()}"/>
                         <label><c:out value="${item.getName()}"/></label>
                         <br/><br/>
                    </c:forEach>
                    <br/>
                    <h3><i>${newMealPrompt}</i></h3>
                    <c:forEach items="${meals}" var="item" varStatus="loop">
                         <input type="radio" name="newMealId" value="${item.getId()}"/>
                         <label><c:out value="${item.getName()}"/></label>
                         <br/><br/>
                    </c:forEach>
                    <br/>
                    <input type="submit" value="Submit" />
               </form>
          </c:if>

          <jsp:include page="/WEB-INF/util/loopItems.jsp" />

          <c:if test="${operationResult ne null}">
               <p><c:out value="${operationResult}"/></p>
          </c:if>

          <br/>
          <a href="<c:url value="/"/>">Return Home</a>
    </body>
</html>