<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <header>
        <link rel="stylesheet" href="/styles/styles.css">
        <title>New Period</title>
    </header>
    <body>
        <h2>New Meal-Period at Therap Canteen</h2>
        <c:if test="${empty periodName}">
            <h3><i><c:out value="${prompt}"/></i></h3>
            <form action = "/newPeriod" method="post">
                <input type="text" name="periodName" value="${periodName}"><br>
                <input type="submit" value="Submit">
            </form>
        </c:if>

        <c:if test="${periodName ne null}">
            <p><c:out value="${operationResult}"/></p>
        </c:if>

        <br/>
        <a href="<c:url value="/"/>">Return Home</a>
    </body>
</html>