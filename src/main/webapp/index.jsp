<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <style>
            html, body {
                height: 100%;
            }
            html {
                display: table;
                margin: auto;
            }
            body {
                display: table-cell;
                vertical-align: middle;
            }
            h1, h2 {
                color: orange;
            }
            table, th, td {
                border: 1px solid black;
                min-width: 500px;
            }
            td {
                text-align: center;
            }
        </style>
        <title>Therap Canteen</title>
    </head>
    <body>
        <h1><i>Welcome to Therap Canteen</i></h1>
        <table>
            <tr>
                <th><h3>All Canteen Operations</h3></th>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/menu"/>">View Menu</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/meals"/>">View All Meals</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/periods"/>">View All Periods</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/mealsForPeriod"/>">View Meals for Period</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/mealsForDay"/>">View Meals for Day</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/periodsForDay"/>">View Periods for Day</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/mealSchedule"/>">View Serving Schedule For Meal</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/newMeal"/>">Insert New Meal</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/mealToDay"/>">Add Meal to Day</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/mealToPeriod"/>">Add Meal to Period</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/mealToCanteenEntry"/>">Add Meal for Day and period</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/updateMeal"/>">Update Meal Name</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/replaceMeal"/>">Replace Meal for Day and Period</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/cancelCanteenEntryForMeal"/>">Cancel Meal for Day and Period</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/cancelMeal"/>">Cancel Meal</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/newPeriod"/>">Insert New Meal-Period</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/cancelPeriodForDay"/>">Cancel Meal-Period for Day</a></h5></td>
            </tr>
            <tr>
                <td><h5><a href="<c:url value="/cancelPeriod"/>">Cancel Meal-Period</a></h5></td>
            </tr>
        </table>
    </body>
</html>