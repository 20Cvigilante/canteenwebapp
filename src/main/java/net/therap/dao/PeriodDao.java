package net.therap.dao;

import net.therap.entity.Period;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.util.List;

import static net.therap.dao.CanteenDao.getEntityManager;

/**
 * @author musa.khan
 * @since 09/12/2020
 */
public class PeriodDao {

    private static final String GET_PERIOD_BY_NAME = "SELECT p " +
            "FROM Period p " +
            "WHERE p.name = :periodName";

    private static final String GET_ALL_PERIODS = "SELECT p " +
            "FROM Period p "+
            "ORDER BY p.name";

    public boolean insertPeriod(Period period) {
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            entityManager.persist(period);

            entityManager.getTransaction().commit();

        } catch (PersistenceException e) {
            e.printStackTrace();

            return false;

        } finally {
            entityManager.close();
        }

        return true;
    }

    public Period getPeriodByName(String periodName) {
        EntityManager entityManager = getEntityManager();

        try {
            return entityManager.createQuery(GET_PERIOD_BY_NAME, Period.class)
                    .setParameter("periodName", periodName)
                    .getSingleResult();

        } catch (PersistenceException e) {
            return null;

        } finally {
            entityManager.close();
        }
    }

    public Period getPeriodById(int periodId) {
        EntityManager entityManager = getEntityManager();

        try {
            return entityManager.find(Period.class, periodId);

        } catch (PersistenceException e){
            return null;

        } finally {
            entityManager.close();
        }
    }

    public List<Period> getAllPeriods() {
        EntityManager entityManager = getEntityManager();

        List<Period> periods =  entityManager.createQuery(GET_ALL_PERIODS, Period.class).getResultList();

        entityManager.close();

        return periods;
    }

    public boolean deletePeriod(Period period) {
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            period = entityManager.getReference(Period.class, period.getId());

            entityManager.remove(period);

            entityManager.getTransaction().commit();

        } catch (PersistenceException e) {
            e.printStackTrace();

            return false;

        } finally {
            entityManager.close();
        }

        return true;
    }
}