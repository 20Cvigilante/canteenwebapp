package net.therap.dao;

import net.therap.entity.Meal;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.util.Collections;
import java.util.List;

import static net.therap.dao.CanteenDao.getEntityManager;

/**
 * @author musa.khan
 * @since 09/12/2020
 */
public class MealDao {

    private static final String GET_MEAL_BY_NAME = "SELECT m " +
            "FROM Meal m " +
            "WHERE m.name = :mealName";

    private static final String GET_ALL_MEALS = "SELECT m " +
            "FROM Meal m "+
            "ORDER BY m.name";

    public boolean insertMeal(Meal meal) {
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            entityManager.persist(meal);

            entityManager.getTransaction().commit();

        } catch (PersistenceException e) {
            e.printStackTrace();

            return false;

        } finally {
            entityManager.close();
        }

        return true;
    }

    public Meal getMealByName(String mealName) {
        EntityManager entityManager = getEntityManager();

        try {
            return entityManager.createQuery(GET_MEAL_BY_NAME, Meal.class)
                    .setParameter("mealName", mealName)
                    .getSingleResult();

        } catch (PersistenceException e){
            return null;

        } finally {
            entityManager.close();
        }
    }

    public Meal getMealById(int mealId) {
        EntityManager entityManager = getEntityManager();

        try {
            return entityManager.find(Meal.class, mealId);

        } catch (PersistenceException e){
            return null;

        } finally {
            entityManager.close();
        }
    }

    public List<Meal> getAllMeals() {
        EntityManager entityManager = getEntityManager();

        try {
            return entityManager.createQuery(GET_ALL_MEALS, Meal.class)
                    .getResultList();

        } catch (PersistenceException e) {
            e.printStackTrace();

            return Collections.emptyList();

        } finally {
            entityManager.close();
        }
    }

    public boolean updateMealName(Meal meal) {
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            entityManager.merge(meal);

            entityManager.getTransaction().commit();

        } catch (PersistenceException e) {
            e.printStackTrace();

            return false;

        } finally {
            entityManager.close();
        }

        return true;
    }

    public boolean deleteMeal(Meal meal) {
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            meal = entityManager.getReference(Meal.class, meal.getId());

            entityManager.remove(meal);

            entityManager.getTransaction().commit();

        } catch (PersistenceException e) {
            e.printStackTrace();

            return false;

        } finally {
            entityManager.close();
        }

        return true;
    }
}