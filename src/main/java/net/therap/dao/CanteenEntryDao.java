package net.therap.dao;

import net.therap.entity.CanteenEntry;
import net.therap.entity.Days;
import net.therap.entity.Meal;
import net.therap.entity.Period;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import java.util.Collections;
import java.util.List;

import static net.therap.dao.CanteenDao.getEntityManager;

/**
 * @author musa.khan
 * @since 09/12/2020
 */
public class CanteenEntryDao {

    private static final String CANTEEN_ENTRIES_FOR_DAY_JPQL = "SELECT cr " +
            "FROM CanteenEntry cr " +
            "LEFT JOIN FETCH cr.period " +
            "LEFT JOIN FETCH cr.meal " +
            "WHERE cr.day = :day";

    private static final String CANTEEN_ENTRIES_FOR_MEAL_JPQL = "SELECT cr " +
            "FROM CanteenEntry cr " +
            "LEFT JOIN FETCH cr.period " +
            "WHERE cr.meal.id = :mealId";

    private static final String GET_SPECIFIC_CANTEEN_ENTRY = "SELECT cr " +
            "FROM CanteenEntry cr " +
            "WHERE cr.period.id = :currentPeriodId AND cr.meal.id = :currentMealId AND cr.day = :currentDay";

    private static final String GET_ALL_SERVED_MEALS = "SELECT distinct(cr.meal) " +
            "FROM CanteenEntry cr "+
            "ORDER BY cr.meal.name";

    private static final String GET_ALL_SERVED_PERIODS = "SELECT distinct(cr.period) " +
            "FROM CanteenEntry cr "+
            "ORDER BY cr.period.name";

    private static final String GET_DAYS_FOR_MEAL = "SELECT distinct(cr.day) " +
            "FROM CanteenEntry cr " +
            "WHERE cr.meal.id = :mealId " +
            "ORDER BY cr.day";

    private static final String GET_MEALS_FOR_DAY = "SELECT distinct(cr.meal) " +
            "FROM CanteenEntry cr " +
            "WHERE cr.day = :day "+
            "ORDER BY cr.meal.name";

    private static final String GET_MEALS_FOR_PERIOD = "SELECT distinct(cr.meal) " +
            "FROM CanteenEntry cr " +
            "WHERE cr.period.id = :periodId "+
            "ORDER BY cr.meal.name";

    private static final String GET_PERIODS_FOR_DAY = "SELECT distinct(cr.period) " +
            "FROM CanteenEntry cr " +
            "WHERE cr.day = :day " +
            "ORDER BY cr.period.name";

    private static final String GET_PERIODS_FOR_DAY_AND_MEAL = "SELECT distinct(cr.period) " +
            "FROM CanteenEntry cr " +
            "WHERE cr.day = :day AND cr.meal.id = :mealId "+
            "ORDER BY cr.period.name";

    private static final String DELETE_PERIOD_FOR_DAY = "DELETE " +
            "FROM CanteenEntry cr " +
            "WHERE cr.day = :day AND cr.period.id = :periodId";

    private static final String GET_DAYS_FOR_PERIOD = "SELECT distinct(cr.day) " +
            "FROM CanteenEntry cr " +
            "WHERE cr.period.id = :periodId " +
            "ORDER BY cr.day";

    public String insertMealToCanteenEntry(CanteenEntry canteenEntry) {
        String operationResult = "";

        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            entityManager.persist(canteenEntry);

            operationResult = String.format("Added for %s and %s.\n", canteenEntry.getDay().naturalName,
                    canteenEntry.getPeriod().getName());

            entityManager.getTransaction().commit();

        } catch (PersistenceException e) {

            if (e.getMessage().contains("ConstraintViolationException")) {
                operationResult = String.format("Already exists for %s and %s.\n", canteenEntry.getDay().naturalName,
                        canteenEntry.getPeriod().getName());
                System.out.print(operationResult);

            } else {
                e.printStackTrace();
            }

            return operationResult;

        } finally {
            entityManager.close();
        }

        return operationResult;
    }

    public List<Days> getDaysForMeal(int mealId) {
        EntityManager entityManager = getEntityManager();

        try {
            return entityManager.createQuery(GET_DAYS_FOR_MEAL, Days.class)
                    .setParameter("mealId", mealId)
                    .getResultList();

        } catch (PersistenceException e) {
            e.printStackTrace();

            return Collections.emptyList();

        } finally {
            entityManager.close();
        }
    }

    public List<Meal> getMealsForPeriod(Period period) {
        EntityManager entityManager = getEntityManager();

        try {
            return entityManager.createQuery(GET_MEALS_FOR_PERIOD, Meal.class)
                    .setParameter("periodId", period.getId())
                    .getResultList();

        } catch (PersistenceException e) {
            e.printStackTrace();

            return Collections.emptyList();

        } finally {
            entityManager.close();
        }
    }

    public List<CanteenEntry> getCanteenEntriesForDay(Days day) {
        EntityManager entityManager = getEntityManager();

        try {
            return entityManager.createQuery(CANTEEN_ENTRIES_FOR_DAY_JPQL, CanteenEntry.class)
                    .setParameter("day", day)
                    .getResultList();

        } catch (PersistenceException | NullPointerException e) {
            e.printStackTrace();

            return Collections.emptyList();

        } finally {
            entityManager.close();
        }
    }

    public List<CanteenEntry> getCanteenEntriesForMeal(Meal meal) {
        EntityManager entityManager = getEntityManager();

        try {
            return entityManager.createQuery(CANTEEN_ENTRIES_FOR_MEAL_JPQL, CanteenEntry.class)
                    .setParameter("mealId", meal.getId())
                    .getResultList();

        } catch (PersistenceException | NullPointerException e) {
            e.printStackTrace();

            return Collections.emptyList();

        } finally {
            entityManager.close();
        }
    }

    public List<Meal> getMealsForDay(Days day) {
        EntityManager entityManager = getEntityManager();

        try {
            return entityManager.createQuery(CanteenEntryDao.GET_MEALS_FOR_DAY, Meal.class)
                    .setParameter("day", day)
                    .getResultList();

        } catch (PersistenceException e) {
            e.printStackTrace();

            return Collections.emptyList();

        } finally {
            entityManager.close();
        }
    }

    public List<Period> getPeriodsForDayAndMeal(Meal meal, Days day) {
        EntityManager entityManager = getEntityManager();

        try {
            return entityManager.createQuery(GET_PERIODS_FOR_DAY_AND_MEAL, Period.class)
                    .setParameter("day", day)
                    .setParameter("mealId", meal.getId())
                    .getResultList();

        } catch (PersistenceException e) {
            e.printStackTrace();

            return Collections.emptyList();

        } finally {
            entityManager.close();
        }
    }

    public List<Period> getPeriodsForDay(Days day) {
        EntityManager entityManager = getEntityManager();

        try {
            return entityManager.createQuery(GET_PERIODS_FOR_DAY, Period.class)
                    .setParameter("day", day)
                    .getResultList();

        } catch (PersistenceException e) {
            e.printStackTrace();

            return Collections.emptyList();

        } finally {
            entityManager.close();
        }
    }

    public List<Days> getDaysForPeriod(int periodId) {
        EntityManager entityManager = getEntityManager();

        try {
            return entityManager.createQuery(GET_DAYS_FOR_PERIOD, Days.class)
                    .setParameter("periodId", periodId)
                    .getResultList();

        } catch (PersistenceException e) {
            e.printStackTrace();

            return Collections.emptyList();

        } finally {
            entityManager.close();
        }
    }

    public List<Meal> getAllServedMeals() {
        EntityManager entityManager = getEntityManager();

        try {
            return entityManager.createQuery(GET_ALL_SERVED_MEALS, Meal.class)
                    .getResultList();

        } catch (PersistenceException e) {
            e.printStackTrace();

            return Collections.emptyList();

        } finally {
            entityManager.close();
        }
    }

    public List<Period> getAllServedPeriods() {
        EntityManager entityManager = getEntityManager();

        try {
            return entityManager.createQuery(GET_ALL_SERVED_PERIODS, Period.class)
                    .getResultList();

        } catch (PersistenceException e) {
            e.printStackTrace();

            return Collections.emptyList();

        } finally {
            entityManager.close();
        }
    }

    public boolean replaceMealForCanteenEntry(Meal currentMeal, Meal newMeal, Days currentDay, Period currentPeriod) {

        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            CanteenEntry canteenEntry = entityManager.createQuery(GET_SPECIFIC_CANTEEN_ENTRY, CanteenEntry.class)
                    .setParameter("currentPeriodId", currentPeriod.getId())
                    .setParameter("currentMealId", currentMeal.getId())
                    .setParameter("currentDay", currentDay)
                    .getSingleResult();

            newMeal = entityManager.merge(newMeal);

            canteenEntry.setMeal(newMeal);

            entityManager.getTransaction().commit();

        } catch (RollbackException e) {
            System.out.printf("Already exists for %s and %s.\n", currentDay.naturalName, currentPeriod.getName());

            return false;

        } catch (PersistenceException e) {
            e.printStackTrace();

            return false;

        } finally {
            entityManager.close();
        }

        return true;
    }

    public boolean deleteCanteenEntryForMeal(Meal currentMeal, Days currentDay, Period currentPeriod) {

        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            CanteenEntry canteenEntry = entityManager.createQuery(GET_SPECIFIC_CANTEEN_ENTRY, CanteenEntry.class)
                    .setParameter("currentPeriodId", currentPeriod.getId())
                    .setParameter("currentMealId", currentMeal.getId())
                    .setParameter("currentDay", currentDay).getSingleResult();

            entityManager.remove(canteenEntry);

            entityManager.getTransaction().commit();

        } catch (PersistenceException e) {
            e.printStackTrace();

            return false;

        } finally {
            entityManager.close();
        }

        return true;
    }

    public boolean deletePeriodForDay(Period period, Days day) {
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            int cancelledCount = entityManager.createQuery(DELETE_PERIOD_FOR_DAY)
                            .setParameter("day", day)
                            .setParameter("periodId", period.getId())
                            .executeUpdate();

            entityManager.getTransaction().commit();

            return cancelledCount > 0;

        } catch (PersistenceException e) {
            e.printStackTrace();

            return false;

        } finally {
            entityManager.close();
        }
    }
}