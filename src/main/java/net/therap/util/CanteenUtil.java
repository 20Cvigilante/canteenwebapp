package net.therap.util;

import net.therap.entity.CanteenEntry;
import net.therap.entity.Days;
import net.therap.entity.Period;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * @author musa.khan
 * @since 06/12/2020
 */
public class CanteenUtil {

    public static final int MAX_DAYS = 5;

    public static String getOperationResult(boolean status) {

        return status ? "Operation successful!" : "Operation not processed!";
    }

    public static List<Days> getAllDays() {

        return new ArrayList<>(Arrays.asList(Days.values()).subList(0, MAX_DAYS));
    }

    public static void processMenuTableData(Days currentDay,
                                            List<String> linesToPrint,
                                            List<CanteenEntry> periodMealsForDay) {

        Period currentPeriod = periodMealsForDay.get(0).getPeriod();

        StringBuilder hourMeals = new StringBuilder();

        for (CanteenEntry canteenEntry : periodMealsForDay) {
            if (canteenEntry.getPeriod().equals(currentPeriod)) {
                hourMeals.append(canteenEntry.getMeal().getName()).append(", ");

            } else {
                linesToPrint.addAll(Arrays.asList(currentDay.naturalName, currentPeriod.getName(),
                        hourMeals.substring(0, hourMeals.length() - 2)));

                currentPeriod = canteenEntry.getPeriod();

                hourMeals = new StringBuilder(canteenEntry.getMeal().getName() + ", ");
            }
        }

        linesToPrint.addAll(Arrays.asList(currentDay.naturalName, currentPeriod.getName(),
                hourMeals.substring(0, hourMeals.length() - 2)));
    }

    public static List<String> processServingScheduleData(List<String> linesToPrint,
                                                          List<CanteenEntry> periodMealsForMeal) {

        String currentDay = periodMealsForMeal.get(0).getDay().naturalName;

        StringBuilder periods = new StringBuilder();

        for (CanteenEntry canteenEntry : periodMealsForMeal) {
            if (!currentDay.equals(canteenEntry.getDay().name())) {

                if (periods.length() > 2) {
                    linesToPrint.addAll(Arrays.asList(currentDay, periods.substring(0, periods.length() - 2)));
                }

                currentDay = canteenEntry.getDay().naturalName;

                periods = new StringBuilder();
            }

            periods.append(canteenEntry.getPeriod().getName()).append(", ");
        }

        if (periods.length() > 2) {
            linesToPrint.addAll(Arrays.asList(currentDay, periods.substring(0, periods.length() - 2)));
        }

        return linesToPrint;
    }

    public static SortCanteenEntriesByPeriod getSortByPeriodComparator() {

        return new SortCanteenEntriesByPeriod();
    }

    public static SortCanteenEntriesByDay getSortByDayComparator() {

        return new SortCanteenEntriesByDay();
    }

    static class SortCanteenEntriesByPeriod implements Comparator<CanteenEntry> {

        public int compare(CanteenEntry a, CanteenEntry b) {

            return a.getPeriod().getName().compareTo(b.getPeriod().getName());
        }
    }

    static class SortCanteenEntriesByDay implements Comparator<CanteenEntry> {

        public int compare(CanteenEntry a, CanteenEntry b) {

            return Integer.compare(a.getDay().ordinal(), b.getDay().ordinal());
        }
    }
}