package net.therap.controller.canteenentry;

import net.therap.controller.AbstractCanteenHttpServlet;
import net.therap.entity.Meal;
import net.therap.entity.Period;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * @author musa.khan
 * @since 20/12/2020
 */
@WebServlet("/mealsForPeriod")
public class MealsForPeriodServlet extends AbstractCanteenHttpServlet {

    private static final String JSP_PATH = "/WEB-INF/view/canteenentry/mealsForPeriod.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer periodId = getEntityIdFromRequest(req, PERIOD_ID_REQ_PARAM);

        if (Objects.isNull(periodId)) {
            setupAttributesForUI(req, PERIOD_ID_REQ_PARAM, GET);

        } else {
            Period period = periodDao.getPeriodById(periodId);
            List<Meal> meals = canteenEntryDao.getMealsForPeriod(period);

            req.setAttribute("periodName", period.getName());
            req.setAttribute("meals", meals);
        }

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}