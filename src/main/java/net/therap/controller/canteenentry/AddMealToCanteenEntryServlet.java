package net.therap.controller.canteenentry;

import net.therap.controller.AbstractCanteenHttpServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

/**
 * @author musa.khan
 * @since 20/12/2020
 */
@WebServlet("/mealToCanteenEntry")
public class AddMealToCanteenEntryServlet extends AbstractCanteenHttpServlet {

    private static final String JSP_PATH = "/WEB-INF/view/canteenentry/mealToCanteenEntry.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setupAttributesForUI(req, MEAL_ID_REQ_PARAM, POST);

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = getSession(req);

        Integer mealId = getEntityIdFromRequest(req, MEAL_ID_REQ_PARAM);
        Integer dayId = getEntityIdFromRequest(req, DAY_ID_REQ_PARAM);
        Integer periodId = getEntityIdFromRequest(req, PERIOD_ID_REQ_PARAM);

        if (Objects.isNull(dayId)) {
            setSessionAttribute(req, MEAL_ID_REQ_PARAM, mealId);
            setupAttributesForUI(req, DAY_ID_REQ_PARAM, POST);

        } else if (Objects.isNull(periodId)) {
            setSessionAttribute(req, DAY_ID_REQ_PARAM, dayId);
            setupAttributesForUI(req, PERIOD_ID_REQ_PARAM, POST);

        } else {
            String operationResult = canteenEntryService.insertMealToCanteenEntry(mealId, dayId, periodId);
            req.setAttribute("operationResult", operationResult);

            session.removeAttribute(req.getServletPath());
        }

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}