package net.therap.controller.canteenentry;

import net.therap.controller.AbstractCanteenHttpServlet;
import net.therap.entity.Days;
import net.therap.entity.Meal;
import net.therap.entity.Period;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static net.therap.util.CanteenUtil.getOperationResult;

/**
 * @author musa.khan
 * @since 20/12/2020
 */
@WebServlet("/deleteCanteenEntryForMeal")
public class DeleteCanteenEntryForMealServlet extends AbstractCanteenHttpServlet {

    private static final String JSP_PATH = "/WEB-INF/view/canteenentry/deleteCanteenEntryForMeal.jsp";

    private static final String mealPrompt = "Select a meal to cancel";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Meal> meals = canteenEntryDao.getAllServedMeals();

        setupAttributesForUI(req, MEAL_ID_REQ_PARAM, POST, mealPrompt, meals);

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = getSession(req);

        Integer mealId = getEntityIdFromRequest(req, MEAL_ID_REQ_PARAM);
        Integer dayId = getEntityIdFromRequest(req, DAY_ID_REQ_PARAM);
        Integer periodId = getEntityIdFromRequest(req, PERIOD_ID_REQ_PARAM);

        if (Objects.isNull(dayId)) {
            setSessionAttribute(req, MEAL_ID_REQ_PARAM, mealId);

            List<Days> days = canteenEntryDao.getDaysForMeal(mealId);
            setupAttributesForUI(req, DAY_ID_REQ_PARAM, POST, days);

        } else if (Objects.isNull(periodId)) {
            setSessionAttribute(req, DAY_ID_REQ_PARAM, dayId);

            List<Period> periodsForDayAndMeal = canteenEntryService.getPeriodsForDayAndMeal(mealId, dayId);
            setupAttributesForUI(req, PERIOD_ID_REQ_PARAM, POST, periodsForDayAndMeal);

        } else {
            boolean status = canteenEntryService.deleteCanteenEntryForMeal(mealId, dayId, periodId);
            String operationResult = getOperationResult(status);
            req.setAttribute("operationResult", operationResult);

            session.removeAttribute(req.getServletPath());
        }

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}