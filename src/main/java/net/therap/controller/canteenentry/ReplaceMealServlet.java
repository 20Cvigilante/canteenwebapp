package net.therap.controller.canteenentry;

import net.therap.controller.AbstractCanteenHttpServlet;
import net.therap.entity.Days;
import net.therap.entity.Meal;
import net.therap.entity.Period;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static net.therap.util.CanteenUtil.getOperationResult;

/**
 * @author musa.khan
 * @since 20/12/2020
 */
@WebServlet("/replaceMeal")
public class ReplaceMealServlet extends AbstractCanteenHttpServlet {

    private static final String JSP_PATH = "/WEB-INF/view/canteenentry/replaceMeal.jsp";

    private static final String currMealPrompt = "Select the current meal to replace";
    private static final String newMealPrompt = "Select the new meal to replace with";
    private static final String dayPrompt = "Select a day for which to replace the current meal:";
    private static final String periodPrompt = "Select a period to replace the meal for:";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Meal> servedMeals = canteenEntryDao.getAllServedMeals();
        List<Meal> meals = mealDao.getAllMeals();

        req.setAttribute("currMealPrompt", currMealPrompt);
        req.setAttribute("newMealPrompt", newMealPrompt);
        req.setAttribute("servedMeals", servedMeals);
        req.setAttribute("meals", meals);
        req.setAttribute("formMethod", POST);

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = getSession(req);

        Integer currMealId = getEntityIdFromRequest(req, CURR_MEAL_ID_REQ_PARAM);
        Integer newMealId = getEntityIdFromRequest(req, NEW_MEAL_ID_REQ_PARAM);

        Integer dayId = getEntityIdFromRequest(req, DAY_ID_REQ_PARAM);
        Integer periodId = getEntityIdFromRequest(req, PERIOD_ID_REQ_PARAM);

        if (Objects.isNull(dayId)) {
            setSessionAttribute(req, CURR_MEAL_ID_REQ_PARAM, currMealId);
            setSessionAttribute(req, NEW_MEAL_ID_REQ_PARAM, newMealId);

            List<Days> days = canteenEntryDao.getDaysForMeal(currMealId);
            setupAttributesForUI(req, DAY_ID_REQ_PARAM, POST, dayPrompt, days);

        }
        else if (Objects.isNull(periodId)) {
            setSessionAttribute(req, DAY_ID_REQ_PARAM, dayId);

            List<Period> periodsForDayAndMeal = canteenEntryService.getPeriodsForDayAndMeal(currMealId, dayId);
            setupAttributesForUI(req, PERIOD_ID_REQ_PARAM, POST, periodPrompt, periodsForDayAndMeal);

        } else {
            boolean status = canteenEntryService.replaceMealForCanteenEntry(currMealId, newMealId, dayId, periodId);
            String operationResult = getOperationResult(status);
            req.setAttribute("operationResult", operationResult);

            session.removeAttribute(req.getServletPath());
        }

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}