package net.therap.controller.canteenentry;

import net.therap.controller.AbstractCanteenHttpServlet;
import net.therap.entity.Days;
import net.therap.entity.Meal;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * @author musa.khan
 * @since 20/12/2020
 */
@WebServlet("/mealsForDay.jsp")
public class MealsForDayServlet extends AbstractCanteenHttpServlet {

    private static final String JSP_PATH = "/WEB-INF/view/canteenentry/mealsForDay.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer dayId = getEntityIdFromRequest(req, DAY_ID_REQ_PARAM);

        if (Objects.isNull(dayId)) {
            setupAttributesForUI(req, DAY_ID_REQ_PARAM, GET);

        } else {
            Days day = Days.values()[dayId];
            List<Meal> meals = canteenEntryDao.getMealsForDay(day);

            req.setAttribute("dayName", day.getName());
            req.setAttribute("meals", meals);
        }

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}