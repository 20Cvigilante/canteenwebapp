package net.therap.controller.canteenentry;

import net.therap.controller.AbstractCanteenHttpServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * @author musa.khan
 * @since 20/12/2020
 */
@WebServlet("/mealToDay")
public class AddMealToDayServlet extends AbstractCanteenHttpServlet {

    private static final String JSP_PATH = "/WEB-INF/view/canteenentry/mealToDay.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setupAttributesForUI(req, MEAL_ID_REQ_PARAM, POST);

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = getSession(req);

        Integer mealId = getEntityIdFromRequest(req, MEAL_ID_REQ_PARAM);
        Integer dayId = getEntityIdFromRequest(req, DAY_ID_REQ_PARAM);

        if (Objects.isNull(dayId)) {
            setSessionAttribute(req, MEAL_ID_REQ_PARAM, mealId);
            setupAttributesForUI(req, DAY_ID_REQ_PARAM, POST);

        } else {
            List<String> operationResults = canteenEntryService.addMealToDay(mealId, dayId);
            req.setAttribute("operationResults", operationResults);

            session.removeAttribute(req.getServletPath());
        }

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}