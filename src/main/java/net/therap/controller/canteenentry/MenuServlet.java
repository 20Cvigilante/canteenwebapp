package net.therap.controller.canteenentry;

import net.therap.controller.AbstractCanteenHttpServlet;
import net.therap.service.CanteenEntryService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * @author musa.khan
 * @since 19/12/2020
 */
@WebServlet("/menu")
public class MenuServlet extends AbstractCanteenHttpServlet {

    private static final String JSP_PATH = "/WEB-INF/view/canteenentry/menu.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        List<String> menuPrintLines = new CanteenEntryService().getAllCanteenEntryData();
        req.setAttribute("menuPrintLines", menuPrintLines);

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}