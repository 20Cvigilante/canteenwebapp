package net.therap.controller.canteenentry;

import net.therap.controller.AbstractCanteenHttpServlet;
import net.therap.entity.Days;
import net.therap.entity.Period;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static net.therap.util.CanteenUtil.getOperationResult;

/**
 * @author musa.khan
 * @since 20/12/2020
 */
@WebServlet("/deletePeriodForDay")
public class DeletePeriodForDayServlet extends AbstractCanteenHttpServlet {

    private static final String JSP_PATH = "/WEB-INF/view/canteenentry/deletePeriodForDay.jsp";

    private static final String periodPrompt = "Select a period to cancel";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Period> periods = canteenEntryDao.getAllServedPeriods();

        setupAttributesForUI(req, PERIOD_ID_REQ_PARAM, POST, periodPrompt, periods);

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = getSession(req);

        Integer periodId = getEntityIdFromRequest(req, PERIOD_ID_REQ_PARAM);
        Integer dayId = getEntityIdFromRequest(req, DAY_ID_REQ_PARAM);

        if (Objects.isNull(periodId)) {
            String periodPrompt = "Select a period to cancel";
            List<Period> periods = canteenEntryDao.getAllServedPeriods();

            setupAttributesForUI(req, PERIOD_ID_REQ_PARAM, periodPrompt, periods);

        } else if (Objects.isNull(dayId)) {
            setSessionAttribute(req, PERIOD_ID_REQ_PARAM, periodId);

            List<Days> days = canteenEntryDao.getDaysForPeriod(periodId);
            setupAttributesForUI(req, DAY_ID_REQ_PARAM, POST, days);

        } else {
            boolean status = canteenEntryService.deletePeriodForDay(periodId, dayId);
            String operationResult = getOperationResult(status);
            req.setAttribute("operationResult", operationResult);

            session.removeAttribute(req.getServletPath());
        }

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}