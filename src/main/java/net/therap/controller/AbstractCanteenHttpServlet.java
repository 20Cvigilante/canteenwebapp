package net.therap.controller;

import net.therap.dao.CanteenEntryDao;
import net.therap.dao.MealDao;
import net.therap.dao.PeriodDao;
import net.therap.service.CanteenEntryService;
import net.therap.service.MealService;
import net.therap.service.PeriodService;
import net.therap.util.CanteenUtil;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author musa.khan
 * @since 19/12/2020
 */
public abstract class AbstractCanteenHttpServlet extends HttpServlet {

    private static final String DEFAULT_MEAL_PROMPT = "Select a meal:";
    private static final String DEFAULT_PERIOD_PROMPT = "Select a period:";
    private static final String DEFAULT_DAY_PROMPT = "Select a day:";
    private static final String DEFAULT_MEAL_NAME_PROMPT = "Provide name for meal (length less than 100 characters):";
    private static final String DEFAULT_PERIOD_NAME_PROMPT =
            "Provide name for the new period (less than 100 characters):";

    protected static final String GET = "get";
    protected static final String POST = "post";

    protected static final String MEAL_ID_REQ_PARAM = "mealId";
    protected static final String CURR_MEAL_ID_REQ_PARAM = "currMealId";
    protected static final String NEW_MEAL_ID_REQ_PARAM = "newMealId";
    protected static final String MEAL_NAME_REQ_PARAM = "mealName";
    protected static final String PERIOD_ID_REQ_PARAM = "periodId";
    protected static final String PERIOD_NAME_REQ_PARAM = "periodName";
    protected static final String DAY_ID_REQ_PARAM = "dayId";

    protected static final MealDao mealDao = new MealDao();
    protected static final PeriodDao periodDao = new PeriodDao();
    protected static final CanteenEntryDao canteenEntryDao = new CanteenEntryDao();

    protected static final MealService mealService = new MealService();
    protected static final PeriodService periodService = new PeriodService();
    protected static final CanteenEntryService canteenEntryService = new CanteenEntryService();

    protected HttpSession getSession(HttpServletRequest req) {
        HttpSession session = req.getSession();

        Map<String, Integer> sessionData = (Map<String, Integer>) session.getAttribute(req.getServletPath());

        if (Objects.isNull(sessionData)) {
            sessionData = new HashMap<>();
            session.setAttribute(req.getServletPath(), sessionData);
            System.out.println("SYSLOG: Session ds initialized for servlet operation.");
        }

        return session;
    }

    protected Integer getEntityIdFromRequest(HttpServletRequest req, String param) {
        HttpSession session = getSession(req);
        Map<String, Integer> sessionData = (Map<String, Integer>) session.getAttribute(req.getServletPath());

        Integer number = sessionData.get(param);

        if (Objects.isNull(number)) {
            try {
                number = Integer.parseInt(req.getParameter(param));
                System.out.println("SYSLOG: Number obtained from REQUEST: " + number);

            } catch (NumberFormatException e) {
                System.out.printf("SYSLOG: %s not selected yet.\n", param);
            }
        } else {
            System.out.println("SYSLOG: Number obtained from SESSION: " + number);
        }

        return number;
    }

    protected void setSessionAttribute(HttpServletRequest req, String param, Integer value) {
        String servletPath = req.getServletPath();
        Map<String, Integer> sessionData = (Map<String, Integer>) req.getSession().getAttribute(servletPath);
        sessionData.put(param, value);
        req.getSession().setAttribute(servletPath, sessionData);
    }

    protected void setupAttributesForUI(HttpServletRequest req, String reqParam, String formMethod) {
        setupAttributesForUI(req, reqParam, formMethod, null, null);
    }

    protected void setupAttributesForUI(HttpServletRequest req,
                                        String reqParam,
                                        String formMethod,
                                        List<?> items) {

        setupAttributesForUI(req, reqParam, formMethod, null, items);
    }

    protected void setupAttributesForUI(HttpServletRequest req,
                                        String reqParam,
                                        String formMethod,
                                        String prompt) {

        setupAttributesForUI(req, reqParam, formMethod, prompt, null);
    }

    protected void setupAttributesForUI(HttpServletRequest req,
                                        String param,
                                        String formMethod,
                                        String prompt,
                                        List<?> items) {

        switch(param) {

            case MEAL_ID_REQ_PARAM:
                if (Objects.isNull(items)) {
                    items = mealDao.getAllMeals();
                }

                if (Objects.isNull(prompt)) {
                    prompt = DEFAULT_MEAL_PROMPT;
                }

                break;

            case DAY_ID_REQ_PARAM:
                if (Objects.isNull(items)) {
                    items = CanteenUtil.getAllDays();
                }

                if (Objects.isNull(prompt)) {
                    prompt = DEFAULT_DAY_PROMPT;
                }

                break;

            case PERIOD_ID_REQ_PARAM:
                if (Objects.isNull(items)) {
                    items = periodDao.getAllPeriods();
                }

                if (Objects.isNull(prompt)) {
                    prompt = DEFAULT_PERIOD_PROMPT;
                }

                break;

            case MEAL_NAME_REQ_PARAM:
                prompt = DEFAULT_MEAL_NAME_PROMPT;

                break;

            case PERIOD_NAME_REQ_PARAM:
                prompt = DEFAULT_PERIOD_NAME_PROMPT;

                break;
        }

        req.setAttribute("valueType", param);
        req.setAttribute("prompt", prompt);
        req.setAttribute("items", items);
        req.setAttribute("action", req.getServletPath());
        req.setAttribute("formMethod", formMethod);
    }
}