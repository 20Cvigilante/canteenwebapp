package net.therap.controller.meal;

import net.therap.controller.AbstractCanteenHttpServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

import static net.therap.util.CanteenUtil.getOperationResult;

/**
 * @author musa.khan
 * @since 20/12/2020
 */
@WebServlet("/deleteMeal")
public class DeleteMealServlet extends AbstractCanteenHttpServlet {

    private static final String JSP_PATH = "/WEB-INF/view/meal/deleteMeal.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setupAttributesForUI(req, MEAL_ID_REQ_PARAM, POST);

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer mealId = getEntityIdFromRequest(req, MEAL_ID_REQ_PARAM);

        if (Objects.nonNull(mealId)) {
            boolean status = mealService.deleteMeal(mealId);
            String operationResult = getOperationResult(status);

            req.setAttribute("operationResult", operationResult);
        }

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}