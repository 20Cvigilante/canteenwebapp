package net.therap.controller.meal;

import net.therap.controller.AbstractCanteenHttpServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;


/**
 * @author musa.khan
 * @since 19/12/2020
 */
@WebServlet("/mealSchedule")
public class MealScheduleServlet extends AbstractCanteenHttpServlet {

    private static final String JSP_PATH = "/WEB-INF/view/meal/mealSchedule.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        Integer mealId = getEntityIdFromRequest(req, MEAL_ID_REQ_PARAM);

        if (Objects.isNull(mealId)) {
            setupAttributesForUI(req, MEAL_ID_REQ_PARAM, GET);

        } else {
            String mealName = mealDao.getMealById(mealId).getName();
            List<String> schedulePrintLines = canteenEntryService.getServingScheduleDataForMeal(mealId);

            req.setAttribute("schedulePrintLines", schedulePrintLines);
            req.setAttribute("mealName", mealName);
        }

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}