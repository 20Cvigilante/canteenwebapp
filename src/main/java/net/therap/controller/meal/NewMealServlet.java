package net.therap.controller.meal;

import net.therap.controller.AbstractCanteenHttpServlet;
import net.therap.entity.Meal;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

import static net.therap.util.CanteenUtil.getOperationResult;

/**
 * @author musa.khan
 * @since 20/12/2020
 */
@WebServlet("/newMeal")
public class NewMealServlet extends AbstractCanteenHttpServlet {

    private static final String JSP_PATH = "/WEB-INF/view/meal/newMeal.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setupAttributesForUI(req, MEAL_NAME_REQ_PARAM, POST);

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String mealName = req.getParameter("mealName");

        if (Objects.isNull(mealName) || mealName.length() >= 100) {
            doGet(req, resp);

        } else {
            Meal meal = mealDao.getMealByName(mealName);

            String operationResult;

            if (Objects.nonNull(meal)) {
                operationResult = "Meal with that name already exists!";

            } else {
                boolean status = mealService.insertNewMeal(mealName);
                operationResult = getOperationResult(status);
            }

            req.setAttribute(MEAL_NAME_REQ_PARAM, mealName);
            req.setAttribute("operationResult", operationResult);
        }

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}