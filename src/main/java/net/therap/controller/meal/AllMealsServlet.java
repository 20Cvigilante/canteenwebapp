package net.therap.controller.meal;

import net.therap.controller.AbstractCanteenHttpServlet;
import net.therap.entity.Meal;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author musa.khan
 * @since 19/12/2020
 */
@WebServlet("/meals")
public class AllMealsServlet extends AbstractCanteenHttpServlet {

    private static final String JSP_PATH = "/WEB-INF/view/meal/meals.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        List<Meal> meals = mealDao.getAllMeals();
        req.setAttribute("meals", meals);

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}