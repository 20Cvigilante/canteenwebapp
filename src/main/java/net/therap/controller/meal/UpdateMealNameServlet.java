package net.therap.controller.meal;

import net.therap.controller.AbstractCanteenHttpServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

import static net.therap.util.CanteenUtil.getOperationResult;

/**
 * @author musa.khan
 * @since 20/12/2020
 */
@WebServlet("/updateMeal")
public class UpdateMealNameServlet extends AbstractCanteenHttpServlet {

    private static final String JSP_PATH = "/WEB-INF/view/meal/updateMeal.jsp";

    private static final String newNamePrompt = "Provide new name for the selected meal (less than 100 characters):";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setupAttributesForUI(req, MEAL_ID_REQ_PARAM, POST);

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = getSession(req);

        Integer mealId = getEntityIdFromRequest(req, MEAL_ID_REQ_PARAM);

        String newName = req.getParameter("newName");

        if (Objects.isNull(newName) || newName.length() >= 100) {
            setSessionAttribute(req, MEAL_ID_REQ_PARAM, mealId);

            req.setAttribute("newNamePrompt", newNamePrompt);

        } else {
            boolean status = mealService.updateMealName(mealId, newName);
            String operationResult = getOperationResult(status);

            req.setAttribute(MEAL_NAME_REQ_PARAM, newName);
            req.setAttribute("operationResult", operationResult);

            session.removeAttribute(req.getServletPath());
        }

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}