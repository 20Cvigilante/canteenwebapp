package net.therap.controller.period;

import net.therap.controller.AbstractCanteenHttpServlet;
import net.therap.entity.Period;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author musa.khan
 * @since 19/12/2020
 */
@WebServlet("/periods")
public class AllPeriodsServlet extends AbstractCanteenHttpServlet {

    protected static final String JSP_PATH = "/WEB-INF/view/period/periods.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        List<Period> periods = periodDao.getAllPeriods();
        req.setAttribute("periods", periods);

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}