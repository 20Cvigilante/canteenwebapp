package net.therap.controller.period;

import net.therap.controller.AbstractCanteenHttpServlet;
import net.therap.entity.Period;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

import static net.therap.util.CanteenUtil.getOperationResult;

/**
 * @author musa.khan
 * @since 20/12/2020
 */
@WebServlet("/newPeriod")
public class NewPeriodServlet extends AbstractCanteenHttpServlet {

    private static final String JSP_PATH = "/WEB-INF/view/period/newPeriod.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setupAttributesForUI(req, PERIOD_NAME_REQ_PARAM, POST);

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String periodName = req.getParameter(PERIOD_NAME_REQ_PARAM);

        if (Objects.isNull(periodName) || periodName.length() >= 100) {
            doGet(req, resp);

        } else {

            Period period = periodDao.getPeriodByName(periodName);

            String operationResult;

            if (Objects.nonNull(period)) {
                operationResult = "Period with that name already exists!";

            } else {
                boolean status = periodService.insertNewPeriod(periodName);

                operationResult = getOperationResult(status);
            }

            req.setAttribute(PERIOD_NAME_REQ_PARAM, periodName);
            req.setAttribute("operationResult", operationResult);
        }

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}