package net.therap.controller.period;

import net.therap.controller.AbstractCanteenHttpServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

import static net.therap.util.CanteenUtil.getOperationResult;

/**
 * @author musa.khan
 * @since 20/12/2020
 */
@WebServlet("/deletePeriod")
public class DeletePeriodServlet extends AbstractCanteenHttpServlet {

    private static final String JSP_PATH = "/WEB-INF/view/period/deletePeriod.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setupAttributesForUI(req, PERIOD_ID_REQ_PARAM, POST);

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer periodId = getEntityIdFromRequest(req, PERIOD_ID_REQ_PARAM);

        if (Objects.nonNull(periodId)) {
            boolean status = periodService.deletePeriod(periodId);
            String operationResult = getOperationResult(status);

            req.setAttribute("operationResult", operationResult);
        }

        getServletContext().getRequestDispatcher(JSP_PATH).forward(req, resp);
    }
}