package net.therap.entity;

/**
 * @author musa.khan
 * @since 27/11/2020
 */
public interface CanteenEntity extends Comparable<CanteenEntity> {

    int getId();

    String getName();
}