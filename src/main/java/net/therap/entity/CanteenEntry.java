package net.therap.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author musa.khan
 * @since 08/12/2020
 */
@Entity
@Table(name = "canteen_entries",
        uniqueConstraints={
        @UniqueConstraint(columnNames = {"day", "period_id", "meal_id"})})
public class CanteenEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.ORDINAL)
    private Days day;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "period_id")
    private Period period;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "meal_id")
    private Meal meal;

    public CanteenEntry() {

    }

    public CanteenEntry(Period period, Meal meal, Days day) {
        this.period = period;
        this.meal = meal;
        this.day = day;
    }

    public Days getDay() {
        return day;
    }

    public void setDay(Days day) {
        this.day = day;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CanteenEntry that = (CanteenEntry) o;

        return day == that.day && period.equals(that.period) && meal.equals(that.meal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(day, period, meal);
    }

    @Override
    public String toString() {
        return "CanteenEntry{" +
                "period=" + period +
                ", meal=" + meal +
                ", day=" + day + '}';
    }
}