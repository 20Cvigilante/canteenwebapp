package net.therap.entity;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author musa.khan
 * @since 27/11/2020
 */
@Entity
@Table(name = "periods")
public class Period implements CanteenEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name", unique = true)
    private String name;

    @OneToMany(mappedBy = "period", orphanRemoval = true)
    private Set<CanteenEntry> canteenEntries;

    public Period() {
        canteenEntries = new LinkedHashSet<>();
    }

    public Period(String name) {
        this();
        this.name = name;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Period that = (Period) o;

        return this.getId() == that.getId() && this.getName().equals(that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public int compareTo(CanteenEntity canteenEntity) {
        return this.getName().compareTo(canteenEntity.getName());
    }
}