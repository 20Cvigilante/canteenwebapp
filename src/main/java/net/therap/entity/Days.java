package net.therap.entity;

/**
 * @author musa.khan
 * @since 07/12/2020
 */
public enum Days {

    SUNDAY("Sunday"),
    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday");

    public String naturalName;

    Days(String naturalName) {
        this.naturalName = naturalName;
    }

    public String getName() {
        return this.naturalName;
    }

    public int getId() {
        return this.ordinal();
    }
}