package net.therap.entity;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author musa.khan
 * @since 27/11/2020
 */
@Entity
@Table(name = "meals")
public class Meal implements CanteenEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name", unique = true)
    private String name;

    @OneToMany(mappedBy = "meal", orphanRemoval = true)
    private Set<CanteenEntry> canteenEntries;

    public Meal() {
        canteenEntries = new LinkedHashSet<>();
    }

    public Meal(String name) {
        this();
        this.name = name;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Meal that = (Meal) o;

        return this.getId() == that.getId() && this.getName().equals(that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public int compareTo(CanteenEntity canteenEntity) {
        return this.getName().compareTo(canteenEntity.getName());
    }
}