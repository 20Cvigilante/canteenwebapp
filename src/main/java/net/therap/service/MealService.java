package net.therap.service;

import net.therap.dao.MealDao;
import net.therap.entity.Meal;

/**
 * @author musa.khan
 * @since 19/12/2020
 */
public class MealService {

    private static final MealDao mealDao = new MealDao();

    public boolean insertNewMeal(String mealName) {
        Meal meal = new Meal(mealName);

        return mealDao.insertMeal(meal);
    }

    public boolean updateMealName(Integer mealId, String newName) {
        Meal targetMeal = mealDao.getMealById(mealId);
        targetMeal.setName(newName);

        return mealDao.updateMealName(targetMeal);
    }

    public boolean deleteMeal(Integer mealId) {
        Meal targetMeal = mealDao.getMealById(mealId);

        return mealDao.deleteMeal(targetMeal);
    }
}