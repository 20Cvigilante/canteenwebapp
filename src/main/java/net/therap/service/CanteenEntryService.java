package net.therap.service;

import net.therap.dao.CanteenEntryDao;
import net.therap.dao.MealDao;
import net.therap.dao.PeriodDao;
import net.therap.entity.CanteenEntry;
import net.therap.entity.Days;
import net.therap.entity.Meal;
import net.therap.entity.Period;
import net.therap.util.CanteenUtil;

import java.util.ArrayList;
import java.util.List;

import static net.therap.util.CanteenUtil.*;

/**
 * @author musa.khan
 * @since 19/12/2020
 */
public class CanteenEntryService {

    private static final CanteenEntryDao canteenEntryDao = new CanteenEntryDao();
    private static final MealDao mealDao = new MealDao();
    private static final PeriodDao periodDao = new PeriodDao();

    public String insertMealToCanteenEntry(Integer mealId,
                                           Integer dayId,
                                           Integer periodId) {

        Meal targetMeal = mealDao.getMealById(mealId);
        Days day = Days.values()[dayId];
        Period period = periodDao.getPeriodById(periodId);

        CanteenEntry canteenEntry = new CanteenEntry(period, targetMeal, day);

        return canteenEntryDao.insertMealToCanteenEntry(canteenEntry);
    }

    public List<String> addMealToDay(Integer mealId, Integer dayId) {
        Meal meal = mealDao.getMealById(mealId);
        Days day = Days.values()[dayId];

        List<String> operationResults = new ArrayList<>();
        List<Period> periods = periodDao.getAllPeriods();

        if (periods.isEmpty()) {
            operationResults.add("No meal periods registered!");

            return operationResults;
        }

        for (Period period : periods) {
            CanteenEntry canteenEntry = new CanteenEntry(period, meal, day);
            operationResults.add(canteenEntryDao.insertMealToCanteenEntry(canteenEntry));
        }

        return operationResults;
    }

    public List<String> addMealToPeriod(Integer mealId, Integer periodId) {
        Meal meal = mealDao.getMealById(mealId);
        Period period = periodDao.getPeriodById(periodId);

        List<String> operationResults = new ArrayList<>();

        for (Days day : Days.values()) {
            CanteenEntry canteenEntry = new CanteenEntry(period, meal, day);
            operationResults.add(canteenEntryDao.insertMealToCanteenEntry(canteenEntry));
        }

        return operationResults;
    }

    public List<String> getAllCanteenEntryData() {
        List<String> linesToPrint = new ArrayList<>();

        for (int i = 0; i < 5; ++i) {
            Days currentDay = Days.values()[i];

            List<CanteenEntry> periodMealsForDay = canteenEntryDao.getCanteenEntriesForDay(currentDay);

            if (periodMealsForDay.isEmpty()) {
                continue;
            }

            periodMealsForDay.sort(getSortByPeriodComparator());

            CanteenUtil.processMenuTableData(currentDay, linesToPrint, periodMealsForDay);
        }

        return linesToPrint;
    }

    public List<String> getServingScheduleDataForMeal(Integer mealId) {
        Meal targetMeal = mealDao.getMealById(mealId);

        List<String> linesToPrint = new ArrayList<>();

        List<CanteenEntry> periodMealsForMeal = canteenEntryDao.getCanteenEntriesForMeal(targetMeal);

        if (periodMealsForMeal.isEmpty()) {
            return linesToPrint;
        }

        periodMealsForMeal.sort(getSortByDayComparator());

        return CanteenUtil.processServingScheduleData(linesToPrint, periodMealsForMeal);
    }

    public List<Period> getPeriodsForDayAndMeal(Integer mealId, Integer dayId) {
        Meal meal = mealDao.getMealById(mealId);
        Days day = Days.values()[dayId];
        return canteenEntryDao.getPeriodsForDayAndMeal(meal, day);
    }

    public boolean replaceMealForCanteenEntry(Integer currMealId,
                                              Integer newMealId,
                                              Integer dayId,
                                              Integer periodId) {

        Meal currMeal = mealDao.getMealById(currMealId);
        Meal newMeal = mealDao.getMealById(newMealId);
        Days day = Days.values()[dayId];
        Period period = periodDao.getPeriodById(periodId);

        return canteenEntryDao.replaceMealForCanteenEntry(currMeal, newMeal, day, period);
    }

    public boolean deletePeriodForDay(Integer periodId, Integer dayId) {
        Period targetPeriod = periodDao.getPeriodById(periodId);
        Days day = Days.values()[dayId];

        return canteenEntryDao.deletePeriodForDay(targetPeriod, day);
    }

    public boolean deleteCanteenEntryForMeal(Integer mealId,
                                             Integer dayId,
                                             Integer periodId) {

        Meal targetMeal = mealDao.getMealById(mealId);
        Days day = Days.values()[dayId];
        Period period = periodDao.getPeriodById(periodId);

        return canteenEntryDao.deleteCanteenEntryForMeal(targetMeal, day, period);
    }
}