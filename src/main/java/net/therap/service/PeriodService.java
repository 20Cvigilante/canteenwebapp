package net.therap.service;

import net.therap.dao.PeriodDao;
import net.therap.entity.Period;

/**
 * @author musa.khan
 * @since 19/12/2020
 */
public class PeriodService {

    private static final PeriodDao periodDao = new PeriodDao();

    public boolean insertNewPeriod(String periodName) {
        Period period = new Period(periodName);

        return periodDao.insertPeriod(period);
    }

    public boolean deletePeriod(Integer periodId) {
        Period period = periodDao.getPeriodById(periodId);

        return periodDao.deletePeriod(period);
    }
}